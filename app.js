require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const User = require("./models/user.js");
const { v4: uuidv4 } = require("uuid");

const { initializeApp } = require("firebase/app");
const {
	getFirestore,
	doc,
	setDoc,
	getDoc,
	collection,
	query,
	where,
	getDocs,
} = require("firebase/firestore");
const {
	getAuth,
	createUserWithEmailAndPassword,
} = require("firebase/auth");

const admin = require("firebase-admin");

admin.initializeApp({
	credential: admin.credential.cert(
		require("./[your_filename].json")
	),
});

// Parse the Firebase config from your .env
const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG);
initializeApp(firebaseConfig);

const db = getFirestore();
const auth = getAuth();

const app = express();
app.use(bodyParser.json());


//endpoints:



app.listen(3000, () => {
	console.log("Server is running on port 3000");
});
