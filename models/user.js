require("dotenv").config();
class User {
    constructor(username, email, password) {
      const baseUrl = process.env.BASE_URL+'/users/'+username;
      this.id = `${baseUrl}`; // Unique identifier
      this.type = 'Person';
      this.preferredUsername = username;
      this.inbox = `${baseUrl}/inbox`; // Where to receive activities
      this.outbox = `${baseUrl}/outbox`; // Where to send activities
      // Additional fields can be added as needed
      // Email and password are not part of ActivityPub but are needed for Firebase
      this.email = email;
      this.password = password;
    }
  }
  
  module.exports = User;
  